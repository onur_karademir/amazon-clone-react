import React from "react";
import { BrowserRouter as Router,Route,Switch } from "react-router-dom";
import "./App.css";
import AllCategories from "./pages/AllCategories";
import Electronics from "./pages/Electronics";
import Home from "./pages/Home";
import PremierPage from "./pages/PremierPage";
import CardPage from "./pages/CardPage";
import * as ROUTES from "./routes/Routes"
import Customer from "./pages/Customer";
import BestSeller from "./pages/BestSeller";
import BestOpportunity from "./pages/BestOpportunity";
import UserPage from "./pages/UserPage";
import PrimePage from "./pages/PrimePage";
import PaymentPage from "./pages/PaymentPage";
import FormePage from "./pages/FormePage";

function App() {
  return (
    <>
    <Router>
      <Switch>
      <Route exact path={ROUTES.HOME}>
         <Home />
      </Route>
      <Route exact path={ROUTES.ALLCATEGORIES}>
         <AllCategories/>
      </Route>
      <Route exact path={ROUTES.ELECTRONICS}>
         <Electronics/>
      </Route>
      <Route exact path={ROUTES.PREMIER}>
         <PremierPage/>
      </Route>
      <Route exact path={ROUTES.PREMIER}>
         <PremierPage/>
      </Route>
      <Route exact path={ROUTES.BASKETPAGE}>
         <CardPage/>
      </Route>
      <Route exact path={ROUTES.CUSTOMER_CLAIM}>
         <Customer/>
      </Route>
      <Route exact path={ROUTES.BEST_SELLER}>
         <BestSeller/>
      </Route>
      <Route exact path={ROUTES.BEST_OPPORTUNITY}>
         <BestOpportunity/>
      </Route>
      <Route exact path={ROUTES.USER}>
         <UserPage/>
      </Route>
      <Route exact path={ROUTES.PRIME}>
         <PrimePage/>
      </Route>
      <Route exact path={ROUTES.PAYMENT}>
         <PaymentPage/>
      </Route>
      <Route exact path={ROUTES.FORME}>
         <FormePage/>
      </Route>
      </Switch>
    </Router>
     </>
  );
}

export default App;
