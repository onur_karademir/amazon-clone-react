import React from 'react';
import { UncontrolledCarousel } from 'reactstrap';
import slide from "../slide1.jpg"
import slideTwo from "../slide2.jpg"
import slideThree from "../slide3.jpg"
const items = [
  {
    src: slide,
    caption: 'Mutfak Gereçleri'
  },
  {
    src:slideTwo ,
    altText: 'Slide 2',
    caption: 'Özel Fırsatlar'
  },
  {
    src: slideThree,
    altText: 'Slide 3',
    caption: 'Prime Video'
  }
];

const Carrosel = () => {
    return (
      <React.Fragment>
        <UncontrolledCarousel items={items} slide={false} fade={false}/>
      </React.Fragment>
    )
}

export default Carrosel
