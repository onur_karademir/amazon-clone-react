import React, { useState } from "react";
import {
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";
import {IconContext} from 'react-icons/lib';
import { Header } from "../components";
import * as ROUTES from "../routes/Routes";
import Logo from "../images/amazon.png";
import {MdAddShoppingCart} from "react-icons/md";
import {FaBars} from "react-icons/fa";
import {AiFillCloseCircle} from "react-icons/ai";
import {BiMap} from "react-icons/bi";
import CategoriesData from "../data/catagoriesData.json"
import PrimeImg from "../images/prime.jpg"
const HeaderContainer = () => {
  const [dropdownOpen, setOpen] = useState(false);
  const toggle = () => setOpen(!dropdownOpen);
  const [userDropdown, setUserDropdown] = useState(false);
  const toggleUserDropdown =()=> setUserDropdown(!userDropdown)
  const [menu, setMenu] = useState(false);
  //const [amazonData, setAmazonData] = useState([]);
  function toggleSideMenu() {
    setMenu(!menu)
  }
  var basketArray = JSON.parse(localStorage.getItem('Basket'))
  const getCount = ()=> {
    if (basketArray) {
      return <small className="item-count">{basketArray.length}</small>
    }else {
      return <small className="item-count">0</small>
    }
  }
  // async function getData() {
  //   const response = await fetch("https://amazon-live-data.p.rapidapi.com/getasin/us/B07WDSD7G7", {
  //     "method": "GET",
  //     "headers": {
  //       "x-rapidapi-key": "91cd41c795msh19b817141a0f11ep1851bfjsnc4c54e8023b8",
  //       "x-rapidapi-host": "amazon-live-data.p.rapidapi.com"
  //     }
  //   })
  //   const data = await response.json();
  //   setAmazonData(data.data);
  //   console.log(data);
  // }
  // useEffect(() => {
  //   getData()
  // }, [])
  return (
    <Header fluid={true}>
      <Header.HeaderRow>
        <Header.HeaderNavItem>
          <Header.HeaderDiv className="logo-div">
            <Header.HeaderNavLink to={ROUTES.HOME}>
              <img src={Logo} className="w-100" alt="Amazon.com"/>
            </Header.HeaderNavLink>
          </Header.HeaderDiv>
          <div className="search-div">
            <ButtonDropdown isOpen={dropdownOpen} toggle={toggle}>
              <DropdownToggle className="dropdown-button" caret>Tüm Kategoriler</DropdownToggle>
              <DropdownMenu>
                <DropdownItem className="drop-link" tag="a" href={ROUTES.ALLCATEGORIES}>Tüm Kategoriler</DropdownItem>
                <DropdownItem className="drop-link" tag="a" href={ROUTES.ELECTRONICS}>Elektronik</DropdownItem>
                <DropdownItem className="drop-link" tag="a" href="/">Bilgisayarlar</DropdownItem>
                <DropdownItem className="drop-link" tag="a" href="/">Kitaplar</DropdownItem>
                <DropdownItem className="drop-link" tag="a" href="/">Moda</DropdownItem>
              </DropdownMenu>
            </ButtonDropdown>
            <Header.Input></Header.Input>
            <Header.Button></Header.Button>
          </div>
          <div className="user-div">
              <small className="user-name"><BiMap/>Aiden</small>
              <ButtonDropdown isOpen={userDropdown} toggle={toggleUserDropdown}>
              <DropdownToggle caret size="sm" className="user-dropdown">Hesaplar ve Listeler</DropdownToggle>
              <DropdownMenu className="user-drop-menu">
                <div className="row">
                <div className="col-md-6" style={{borderRight:"1px solid black"}}>
                <DropdownItem className="drop-link" tag="a" href={ROUTES.USER}>Hesabım</DropdownItem>
                <DropdownItem className="drop-link" tag="a" href="/">Siparişlerim</DropdownItem>
                <DropdownItem className="drop-link" tag="a" href={ROUTES.FORME}>Benim İçin Önerilenler</DropdownItem>
                </div>
                <div className="col-md-6">
                <DropdownItem className="drop-link" tag="a" href="/">Hesabım</DropdownItem>
                <DropdownItem className="drop-link" tag="a" href="/">Siparişlerim</DropdownItem>
                <DropdownItem className="drop-link" tag="a" href={ROUTES.FORME}>Benim İçin Önerilenler</DropdownItem>
                </div>

                </div>
              </DropdownMenu>
            </ButtonDropdown>
          </div>
          <Header.HeaderDiv className="basket-div">
          <Header.HeaderNavLink to={ROUTES.BASKETPAGE} className="asd">
          <MdAddShoppingCart className="basket-icon"/>
            <small className="basket-text">Alışveriş <span>Sepeti</span></small>
              {getCount()}
            </Header.HeaderNavLink>
          </Header.HeaderDiv>
        </Header.HeaderNavItem>
      </Header.HeaderRow>
      <Header.HeaderRow>
        <div className={menu ?'side-menu-open':'side-menu'}>
        <IconContext.Provider value={{color:'white'}}>
        <button className="close-side-menu-btn" onClick={toggleSideMenu}><AiFillCloseCircle/></button>
        </IconContext.Provider>
        <div className="side-menu-header">
          <p>Merhaba, Aiden</p>
        </div>
        <li><h3>Öne Çıkanlar</h3></li>
        {CategoriesData.map((categories)=> (
          <li key={categories.id}><a key={categories.title} className="side-menu-link" href={categories.href}>{categories.title}</a></li>
        ))}
        </div>
        <div className="bottom-div">
          <IconContext.Provider value={{color:'white'}}>
          <button className="side-menu-btn" onClick={toggleSideMenu}><FaBars/></button>
          </IconContext.Provider>
          <li><a href={ROUTES.BEST_SELLER}>Çok Satanlar</a></li>
          <li><a href={ROUTES.ELECTRONICS}>Elektronik</a></li>
          <li><a href={ROUTES.BEST_OPPORTUNITY}>Günün Fırsatları</a></li>
          <li><a href={ROUTES.CUSTOMER_CLAIM}>Müşteri Hizmetleri</a></li>
          <li className="d-md-none d-sm-block"><a href={ROUTES.PRIME}>Amazon Prime</a></li>
          <div className="prime-div d-none d-md-block">
          <span><a href={ROUTES.PRIME}><img className="prime-img img img-responsive" src={PrimeImg} alt="prime-img"/></a></span>
          </div>
        </div>
      </Header.HeaderRow>
    </Header>
  );
};

export default HeaderContainer;
