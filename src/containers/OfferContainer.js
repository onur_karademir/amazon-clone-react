import React, { Component } from 'react'
import {Offer} from "../components";
import OfferData from "../data/offerData.json"
class OfferContainer extends Component {
    render() {
        return (
            <Offer>
                <Offer.Inner>
                {OfferData.map((item)=> (
                <Offer.Item key={item.id}>
                    <Offer.Title>{item.title}</Offer.Title>
                    <Offer.Image src={item.img} alt={item.title}></Offer.Image>
                    <Offer.SubTitle>Ürünleri Keşfet</Offer.SubTitle>
                </Offer.Item>
                ))}
                </Offer.Inner>
            </Offer>
        )
    }
}

export default OfferContainer
