import React, { Component } from "react";
import Data from "../data/data"
import *as ROUTER from "../routes/Routes" 
class cardContainer extends Component {
  render() {
    var basketArray = JSON.parse(localStorage.getItem('Basket'))
    const deleteHendler = (e,item) => {
      var basketArray = JSON.parse(localStorage.getItem('Basket'))
      basketArray = basketArray.filter(element => {
       console.log("ee",e.id);
        return e.id !== element.id
      })
      console.log("basket array",basketArray);
      localStorage.setItem("Basket",JSON.stringify(basketArray))
    }
    const removeAllItem = ()=> {
      localStorage.removeItem("Basket")
    }
    const renderElement = (item) => {
      if (basketArray && basketArray.length > 0) {
        return <div className="basket-offering-div mt-3">
        <h3>Sepetiniz <a href="/nav-card" className="btn-sm btn btn-outline-danger all-delete-btn" onClick={removeAllItem}>Bütün Sepeti Sil</a> <a href={ROUTER.PAYMENT} className="btn-sm btn btn-outline-success payment-btn">Ödeme</a></h3>
        <h3>Sepetinizdeki ürünlerin listesi ({basketArray.length} ürün)</h3>
        {basketArray.map((item)=> (
             <div key={item.id} className="basket-offering-item my-3">
                <div className="offering-img-div">
                  <img className="offering-img" src={item.img} alt={item.name}></img>
                </div>
                <div className="offering-text-div">
                  <p className="offering-text">{item.name}</p>
                  <p className="offering-text"><small>Fiyat: </small>{item.price}</p>
                  <p className="offering-text"><small>Puan: </small>{item.rating}</p>
                  <p className="offering-text"><small>Görüntülenme: </small>{item.numReviews}</p>
                  <small className="text-danger font-italic font-weight-bold">Sepete eklendi.</small>
                  <select>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                  </select>
                  <a href="/nav-card" onClick={(e)=> deleteHendler(item)} className="btn btn-danger btn-sm delete-btn">Ürünü Kaldır</a>
                </div>
             </div> 
            ))}
        </div>
      }else if (basketArray && basketArray.length <= 0){
        return <>
        <div className="basket-item-div">
            <h3>Amazon sepetiniz boş.</h3>
            <p>Sonrası için Kaydedilmiş öğelerinizi aşağıdan kontrol edebilirsiniz veya <a href="/">alışverişe devam edin.</a></p>
            </div> 
        <div className="basket-offering-div mt-3">
        <h3>Daha sonrası için kaydedildilenler... ({Data.length} ürün)</h3>
        {Data.map((item)=> (
             <div key={item.id} className="basket-offering-item my-3">
                <div className="offering-img-div">
                  <img className="offering-img" src={item.image} alt={item.name}></img>
                </div>
                <div className="offering-text-div">
                  <p className="offering-text">{item.name}</p>
                  <p className="offering-text"><small>Fiyat: </small>{item.price}</p>
                  <p className="offering-text"><small>Puan: </small>{item.rating}</p>
                  <p className="offering-text"><small>Görüntülenme: </small>{item.numReviews}</p>
                  <small className="text-danger font-italic font-weight-bold">Şu anda mevcut değil.</small>
                </div>
             </div> 
            ))}
        </div>
        </>
      }else {
        return <>
        <div className="basket-item-div">
            <h3>Amazon sepetiniz boş.</h3>
            <p>Sonrası için Kaydedilmiş öğelerinizi aşağıdan kontrol edebilirsiniz veya <a href="/">alışverişe devam edin.</a></p>
            </div> 
        <div className="basket-offering-div mt-3">
        <h3>Daha sonrası için kaydedildilenler... ({Data.length} ürün)</h3>
        {Data.map((item)=> (
             <div key={item.id} className="basket-offering-item my-3">
                <div className="offering-img-div">
                  <img className="offering-img" src={item.image} alt={item.name}></img>
                </div>
                <div className="offering-text-div">
                  <p className="offering-text">{item.name}</p>
                  <p className="offering-text"><small>Fiyat: </small>{item.price}</p>
                  <p className="offering-text"><small>Puan: </small>{item.rating}</p>
                  <p className="offering-text"><small>Görüntülenme: </small>{item.numReviews}</p>
                  <small className="text-danger font-italic font-weight-bold">Şu anda mevcut değil.</small>
                </div>
             </div> 
            ))}
        </div>
        </>
      }
    }
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-8 mt-3">
            {renderElement()}
          </div>
          <div className="col-md-4 mt-3">
            <div className="offering-div">
                <p className="offering-title">Tüm ürünlere yönelik öneriler:</p>
                {Data.map((item)=> (
                 <div key={item.id} className="offering-item">
                    <div className="offering-img-div">
                      <img className="offering-img" src={item.image} alt={item.name}></img>
                    </div>
                    <div className="offering-text-div">
                      <p className="offering-text">{item.name}</p>
                      <p className="offering-text"><small>Fiyat: </small>{item.price}</p>
                      <p className="offering-text"><small>Puan: </small>{item.rating}</p>
                      <p className="offering-text"><small>Görüntülenme: </small>{item.numReviews}</p>
                    </div>
                 </div> 
                ))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default cardContainer;
