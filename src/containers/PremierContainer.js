import React,{useState} from 'react';
import {
    ButtonDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
  } from "reactstrap";
import {Premier} from "../components";
import {MdAddShoppingCart} from "react-icons/md";
const PremierContainer = () => {
    const [dropdownOpen, setOpen] = useState(false);
    const toggle = () => setOpen(!dropdownOpen);
    const sessionStorageDATA = JSON.parse(localStorage.getItem("Product"));
    var basketArray = [];
    function itemHendler(item,e) {
        var storageBasket = JSON.parse(localStorage.getItem('Basket'))
        if (storageBasket) {
            basketArray = storageBasket
        }
        basketArray.push({
            id: `${item.id}`,
            name: `${item.name}`,
            img: `${item.img}`,
            brand: `${item.brand}`,
            price:`${item.price}`,
            rating:`${item.rating}`,
            numReviews:`${item.numReviews}`,
            subTitle:`${item.subTitle}`
        })
        localStorage.setItem("Basket", JSON.stringify(basketArray));
        alert("Ürün Sepete Eklendi...")       
    }
    return (
        <Premier>
            {sessionStorageDATA.map((item)=>(
            <div className="row" key={item.id}>
                <div className="col-md-4">
                    <Premier.ImageDiv>
                        <Premier.Image src={item.img} alt ={item.name}></Premier.Image>
                    </Premier.ImageDiv>
                </div>
                <div className="col-md-4">
                    <Premier.TextDiv>
                        <Premier.Title>{item.name}</Premier.Title>
                        <p className="brand">{item.brand}</p>
                    </Premier.TextDiv>
                    <Premier.TextDiv>
                        <Premier.SubTitle>Renk:Siyah</Premier.SubTitle>
                        <select>
                            <option value="Seç">Seç</option>
                            <option value="S">S</option>
                            <option value="M">M</option>
                            <option value="L">L</option>
                            <option value="XL">XL</option>
                        </select>
                        <p className="price">Fiyat: {item.price}</p>
                        <Premier.Title className="my-3">Açıklama</Premier.Title>
                        <Premier.Text>{item.subTitle}</Premier.Text>
                    </Premier.TextDiv>       
                </div>
                <div className="col-md-4 py-4">
                    <Premier.BasketDiv>
                    <Premier.Text className="my-3 text-center font-weight-bold">Satın Almak İçin Beden Seçin</Premier.Text>
                        <Premier.BasketButton onClick={()=> itemHendler(item)} href="/premier"><MdAddShoppingCart size='30px' className="basket-icon-premier"/>Sepete Ekle</Premier.BasketButton>
                 <ButtonDropdown isOpen={dropdownOpen} toggle={toggle} className="w-100 my-3 bb">
                 <DropdownToggle className="add-list-btn" caret>Listeye Ekle</DropdownToggle>
                 <DropdownMenu className="w-100 text-center">
                  <DropdownItem className="add-list-link" tag="a" href="/premier">+ Liste Oluştur</DropdownItem>
                 </DropdownMenu>
                 </ButtonDropdown>
                    </Premier.BasketDiv>
                </div>
            </div>

            ))}
        </Premier>
    )
}

export default PremierContainer
