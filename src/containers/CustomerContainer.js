import React, { Component } from "react";
import { Customer, Categories } from "../components";
import CustomerData from "../data/CustomerClaimData.json"
class CustomerContainer extends Component {
    constructor (props) {
        super(props);
        this.state = {
            name:"",
            lastName:"",
            content: ""
        }
    }
     submitHendler = (e)=> {
      e.preventDefault();
      alert("Mesajınız başarılı bir şekilde gönderildi...");
      this.setState({
        name:"",
        lastName:"",
        content:""
      })
      console.log(this.state);
  }
  changeHendler = (e)=> {
        this.setState({
            [e.target.name]:e.target.value
        })
  }
  render() {
      const {name,lastName,content} = this.state
    return (
      <>
        <Customer>
          <Customer.WarningDiv>
            <Customer.SubTitle>
              Taleplerde yaşanan artış nedeniyle sipariş onay e-posta
              mesajlarının tarafınıza ulaşması ya da ödemenin onaylanarak
              siparişin güncellenmesi 24 saati bulabilir. Gönderiniz, sipariş
              onay e-postasında belirtilen tahmini teslim tarihine göre size
              ulaştırılacaktır. Bu durumdan kaynaklanabilecek tüm
              rahatsızlıklardan dolayı özür dileriz.
            </Customer.SubTitle>
          </Customer.WarningDiv>
          <Customer.ExplainDiv>
            <Customer.Title>Size yardımcı olmaya hazırız, Aiden</Customer.Title>
            <Customer.Text>
              Burada çoğu şeyi düzeltmeniz için sizi yönlendiririz veya daha
              fazla yardıma ihtiyacınız olursa sizi bir kişiye bağlarız.
            </Customer.Text>
          </Customer.ExplainDiv>
        </Customer>
        <Categories>
          <h3 className="text-center my-5">Bugün size hangi konularda yardımcı olabiliriz?</h3>
          {CustomerData.map((item) => (
            <Categories.Div key={item.id}>
                <img className="customer-icons" src={item.img} alt={item.title}/>
              <Categories.Title>{item.title}</Categories.Title>
            </Categories.Div>
          ))}
        </Categories>
         <div className="customer-form">
             <h4>Bize direkt ulaşın :)</h4>
                <form className="customer-form" onSubmit={this.submitHendler}>
                    <div className="w-100">
                    <input value={name} onChange={this.changeHendler} type="text" id="1" name="name" placeholder="İsim" />
                    <input value={lastName} onChange={this.changeHendler} type="text" id="2" name="lastName" placeholder="Soy İsim" />
                    </div>
                    <textarea rows="4" cols="45" value={content} onChange={this.changeHendler} name="content" placeholder="Mesajınız..."></textarea>
                    <button type="submit" className="mt-3 btn-sm btn btn-primary">Gönder</button>
                </form>
        </div>
      </>
    );
  }
}

export default CustomerContainer;
