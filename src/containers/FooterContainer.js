import React from 'react'
import { Footer } from '../components'
import Logo from "../images/amazon.png";
import {bounce} from "react-animations"
import styled,{keyframes} from "styled-components"
const Bounce = styled.button`
animation:2s ${keyframes `${bounce}`};
`;
const FooterContainer = () => {
    window.onscroll = function() {scrollFunction()};
    function addAnimation() {
        var btn = document.querySelector(".scrool-top-btn")
        btn.classList.add("animate__animated","animate__bounce")
    }
    function removeAnimation() {
        var btn = document.querySelector(".scrool-top-btn")
        btn.classList.remove("animate__animated","animate__bounce")
    }
    function scrollFunction() {
        if (document.body.scrollTop > 300 || document.documentElement.scrollTop > 300) {
            document.querySelector(".scrool-top-btn").style.display = "block";
        } else {
            document.querySelector(".scrool-top-btn").style.display = "none";
        }
    }
    function scroolTop() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }
    return (
        <Footer fluid={true}>
            <div className="container">
                <div className="row footer-top-div">
                <Bounce onMouseLeave={removeAnimation} onMouseOver={addAnimation} className="scrool-top-btn" onClick={scroolTop}>Yukarı Çık
                </Bounce>
                    <div className="col-6 col-md-3">
                            <ul className="footer-list">
                                <h5 className="footer-list-title">Hakkımızda</h5>
                                <li className="footer-list-item">Kariyer</li>
                                <li className="footer-list-item">İletişim</li>
                                <li className="footer-list-item">Bilgi Toplumu Hizmetleri</li>
                            </ul>
                    </div>
                    <div className="col-6 col-md-3">
                            <ul className="footer-list">
                                <h5 className="footer-list-title">Bizimle Para Kazan</h5>
                                <li className="footer-list-item">Ödemeler Hakkında</li>
                                <li className="footer-list-item">Satış Yapın</li>
                            </ul>
                    </div>
                    <div className="col-6 col-md-3">
                            <ul className="footer-list">
                                <h5 className="footer-list-title">Amazon Ödeme Araçları</h5>
                                <li className="footer-list-item">Kredi Kartı</li>
                                <li className="footer-list-item">Taksitli Ödeme</li>
                            </ul>
                    </div>
                    <div className="col-6 col-md-3">
                            <ul className="footer-list">
                                <h5 className="footer-list-title">Size Yardımcı Olalım</h5>
                                <li className="footer-list-item">COVID-19 ve Amazon</li>
                                <li className="footer-list-item">Geri Dönüşüm</li>
                                <li className="footer-list-item">İadeler</li>
                                <li className="footer-list-item">Müşteri Hizmetleri</li>
                                <li className="footer-list-item">Amazon Mobil Uygulaması</li>
                                <li className="footer-list-item">Teslimat Ücretleri ve Politikaları</li>
                            </ul>
                    </div>
                </div>
            </div>
                <div className="footer-bottom-div">
                <div className="footer-img-div">
                    <img src={Logo} className="w-100" alt="Amazon.com"/>
                </div>
                <div className="footer-text">
                    <small>Avustralya</small>
                    <small>Abd</small>
                    <small>Kanada</small>
                    <small>Çin</small>
                    <small>Türkiye</small>
                    <small>Fransa</small>
                    <small>Almanya</small>
                    <small>İtalya</small>
                    <small>Hollanda</small>
                </div>
            </div>
        </Footer>
    )
}

export default FooterContainer
