import React, { Component } from 'react';
import {Forme} from "../components";
import Data from "../data/electronicData.js";

class FormeContainer extends Component {
    render() {
        return (
            <Forme fluid={true}>
                <h3 className="p-5">Daha Keşfedecek Çok Şey Var...</h3>
                {Data.map((item)=> (
                    <Forme.Div key={item.id}>
                        <Forme.Title>{item.name}</Forme.Title>
                        <Forme.Image src={item.image} alt={item.name}></Forme.Image>
                        <Forme.SubTitle>{item.category}</Forme.SubTitle>
                    </Forme.Div>
                ))}
            </Forme>
        )
    }
}

export default FormeContainer
