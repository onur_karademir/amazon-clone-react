import React, { Component } from "react";

class PrimeContainer extends Component {
  render() {
    return (
        <>
        <div className="hero-top-title">
                <h5 className="text-center">Tüm Amazon Prime ayrıcalıkları için şimdi üye ol. Ayda sadece ₺7,90.</h5>
        </div>
      <div className="hero-container">
        <div className="hero-text-div">
          <h2 className="hero-title">Bedava ve hızlı kargo, filmler ve daha fazlası</h2>
          <p className="hero-text">Amazon Prime üyeleri bedava ve hızlı kargo, film ve dizi izleme, özel fırsatlar ve daha pek çok ayrıcalıktan yararlanır. Amazon Prime'ı hemen keşfet.</p>
          <button className="hero-button">30 Gün Ücretsiz Dene</button>
          <small className="hero-small">Ücretsiz deneme süresi sona erdiğinde, Amazon Prime ayda sadece ₺7,90. Dilediğin zaman iptal edebilirsin.</small>
        </div>
      </div>
      <div className="hero-center-container">
      <div className="hero-center-div">
          <div className="hero-center-text-div">
                  <h2 className="hero-title">Siparişin ertesi gün kapında</h2>
                  <p>İstanbul, Bursa, Kocaeli, Ankara, İzmir ve seçili diğer şehirlerde* minimum sipariş tutarı olmaksızın, geçerli ürünlerde kargo bedava, hem de ertesi gün teslimat avantajıyla.</p>
                  <small>*Eskişehir, Antalya, Balıkesir, Bilecik, Bolu, Burdur, Isparta, Kütahya, Manisa, Sakarya, Yalova, Düzce, Afyon</small>
                  <p className="hero-small">Prime ile bedava ve hızlı kargoyu keşfet...</p>
          </div>
      </div>
      <div className="hero-center-div">
          <div className="hero-center-text-div">
                  <h2 className="hero-title">Popüler filmler, ödüllü diziler</h2>
                  <p>Popüler filmleri ve ödüllü Amazon Originals dizilerini, üyeliğin sayesinde ek ücret ödemeden Prime Video'da Türkçe altyazılı izle. Dilersen televizyondan, tabletten veya mobil cihazlarından çevrim içi izle, dilersen indir istediğin zaman istediğin yerden izle.</p>
                  <small>Amazona özel içerikleri keşfetme fırsatı...</small>
                  <p className="hero-small">Prime Video'yu keşfet...</p>
          </div>
      </div>
      </div>
      <div className="cargo-container">
      <div className="cargo-text-div">
          <h2 className="hero-title">Bedava ve hızlı kargo ayrıcalığı</h2>
          <p className="hero-text">Amazon Prime ile geçerli ürünlerdeki alışverişlerinde bedava ve hızlı teslimat ayrıcalığını yaşa. Siparişlerin hangi şehirde ne kadar sürede teslim edilir, hemen öğren.</p>
          <small className="hero-small">Prime ile bedava ve hızlı kargoyu keşfet...</small>
        </div>
      </div>
      <div className="special-container">
      <div className="cargo-text-div">
          <h2 className="hero-title">Özel fırsatları yakala</h2>
          <p className="hero-text">"Günün Fırsatları" sayfasındaki seçili fırsatları herkesten önce sen yakala. Ayrıca, sadece Prime üyelerine özel indirimlerden ve fırsat günlerinden yararlan.</p>
          <small className="hero-small">Fırsatları yakala...</small>
        </div>
      </div>
      </>
    );
  }
}

export default PrimeContainer;
