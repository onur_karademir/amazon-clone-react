import React, { Component } from 'react'
import { User } from '../components'
import userPageData from "../data/userPageData.json"

class UserContainer extends Component {
    render() {
        return (
           <User>
               <h4>Hesabım</h4>
               {userPageData.map((item)=> (
               <User.ItemDiv key={item.id}>
                   <div className="user-img-div">
                   <User.ItemImage src={item.img} alt={item.title}></User.ItemImage>
                   </div>
                   <User.ItemTitle>{item.title}</User.ItemTitle>
                   <User.ItemSubtitle>{item.title}</User.ItemSubtitle>
               </User.ItemDiv>
               ))}
           </User>
        )
    }
}

export default UserContainer
