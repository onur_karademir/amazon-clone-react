import React, { Component } from 'react'
import {Slide} from '../components';
import {AiOutlineLeft,AiOutlineRight} from "react-icons/ai"
import SlideData from "../data/slideData.json"

class SlideContainer extends Component {
    render() {
        function scroolRight() {
            document.querySelector('#slide').scrollLeft += 200;  
        }
        function scroolLeft() {
            document.querySelector('#slide').scrollLeft -= 200;  
        }
        return (
            <Slide>
                <Slide.Inner id="slide">
                    <button className="left-btn" onClick={scroolLeft}><AiOutlineLeft size='20px'/></button>
                    <button className="right-btn" onClick={scroolRight}><AiOutlineRight size='20px'/></button>
                {SlideData.map((item)=> (
                    <Slide.Item key={item.id}>
                    <Slide.Image src={item.img} alt={item.title}></Slide.Image>
                </Slide.Item>
                ))}
                </Slide.Inner>
            </Slide>
        )
    }
}

export default SlideContainer
