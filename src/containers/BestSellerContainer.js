import React, { Component } from 'react'
import CategoriesData from "../data/catagoriesData.json"
import bsElectronicData from "../data/bsElectronicData.js"
import bsFasionData from "../data/bsFasionData.js"
import { BestSeller, Product } from '../components'
class BestSellerContainer extends Component {
    render() {
        return (
            <Product fluid={true} className="electronics-page-product">
            <div className="row">
               <div className="col-md-3 d-none d-lg-block">
                  <div className='navigation-menu'>
                      <li><h3>Menu</h3></li>
                      {CategoriesData.map((categories)=> (
                      <li key={categories.id}><a key={categories.title} className="side-menu-link" href={categories.href}>{categories.title}</a></li>
                      ))}
                   </div>
               </div>
               <div className="col-sm-12 col-lg-9">
                  <BestSeller>
                      <BestSeller.BestSellerDiv>
                          <div className="py-3">
                          <BestSeller.BestSellerTitle>Elektronik Kategorisinde Çok Satanlar</BestSeller.BestSellerTitle>
                          </div>
                            {bsElectronicData.map((item)=> (
                                <BestSeller.BestSellerItem key={item.id}>
                                    <BestSeller.BestSellerNumber>{item.id}.</BestSeller.BestSellerNumber>
                                    <BestSeller.BestSellerImage src={item.image} alt={item.name}></BestSeller.BestSellerImage>
                                    <BestSeller.BestSellerSubTitle>{item.name}</BestSeller.BestSellerSubTitle>
                                </BestSeller.BestSellerItem>
                            ))}
                      </BestSeller.BestSellerDiv>
                      <BestSeller.BestSellerDiv>
                          <div className="py-3">
                          <BestSeller.BestSellerTitle>Moda Kategorisinde Çok Satanlar</BestSeller.BestSellerTitle>
                          </div>
                            {bsFasionData.map((item)=> (
                                <BestSeller.BestSellerItem key={item.id}>
                                    <BestSeller.BestSellerNumber>{item.id}.</BestSeller.BestSellerNumber>
                                    <BestSeller.BestSellerImage src={item.image} alt={item.name}></BestSeller.BestSellerImage>
                                    <BestSeller.BestSellerSubTitle>{item.name}</BestSeller.BestSellerSubTitle>
                                </BestSeller.BestSellerItem>
                            ))}
                      </BestSeller.BestSellerDiv>
                  </BestSeller>
               </div>
            </div>
           </Product>
        )
    }
}

export default BestSellerContainer
