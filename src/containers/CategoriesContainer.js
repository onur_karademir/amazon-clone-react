import React, { Component } from "react";
import { Categories } from "../components";
import categoriesData from "../data/catagoriesData.json";
class CategoriesContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      lastName: "",
    };
  }
  // submitHendler = (e)=> {
  //     e.preventDefault();
  //     console.log('====================================');
  //     console.log(this.state);
  //     console.log('====================================');
  // }
  // changeHendler = (e)=> {
  //       this.setState({
  //           [e.target.name]:e.target.value
  //       })
  // }
  render() {
      //const {name,lastName} = this.state
    return (
      <Categories>
        <h3 className="text-center my-5">Tüm Kategoriler</h3>
        {categoriesData.map((item) => (
          <Categories.Div key={item.id}>
            <Categories.Title>{item.title}</Categories.Title>
          </Categories.Div>
        ))}
        {/* <div className="p-5">
                <form onSubmit={this.submitHendler}>
                    <input value={name} onChange={this.changeHendler} type="text" id="1" name="name" placeholder="name" />
                    <input value={lastName} onChange={this.changeHendler} type="text" id="2" name="lastName" placeholder="lastName" />
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
        </div> */}
      </Categories>
    );
  }
}

export default CategoriesContainer;
