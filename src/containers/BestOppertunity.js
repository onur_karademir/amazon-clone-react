import React, { Component } from 'react'
import { Product } from '../components'
import bsOpportunty from "../data/bsOpportunty.json"
import electronicData from "../data/electronicData.js"
import CategoriesData from "../data/catagoriesData.json"
import FashionData from "../data/data"
class BestOppertunity extends Component {
    render() {
        let productArray = [];
        function setProduct (item) {
                productArray.push({
                    id: `${item.id}`,
                    name: `${item.name}`,
                    img: `${item.image}`,
                    brand: `${item.brand}`,
                    price:`${item.price}`,
                    rating:`${item.rating}`,
                    numReviews:`${item.numReviews}`,
                    subTitle:`${item.subTitle}`
                })
                localStorage.setItem("Product", JSON.stringify(productArray));
        }
        return (
            <div className="container-fluid">
                <h5>Fırsatlar</h5>
                <small>“Bitmeden Yakala!” fırsatlarını ve çok daha fazlasını bu sayfada bulabilirsiniz.</small>
                <div className="w-100 text-center py-3">
                    {bsOpportunty.map((item)=> (
                        <div key={item.id} className="opportunty-div">
                            <img src={item.img} alt={item.title}/>
                            <small>{item.title}</small>
                        </div>
                    ))}
                </div>
                <Product fluid={true} className="electronics-page-product">
            <div className="row">
               <div className="col-md-3 d-none d-lg-block">
                  <div className='navigation-menu'>
                      <li><h3>Menu</h3></li>
                      {CategoriesData.map((categories)=> (
                      <li key={categories.id}><a key={categories.title} className="side-menu-link" href={categories.href}>{categories.title}</a></li>
                      ))}
                   </div>
               </div>
        <div className="col-12 col-sm-12 col-md-12 col-lg-9 text-center">
        {electronicData.map((item)=>(
            <a key={item.id} href="/premier" className="div-link-main-page">
            <Product.Div className='electronic-product' onClick={()=> setProduct(item)}>
                <Product.Title>{item.name}</Product.Title>
                <div className="product-img-div">
                <Product.Image src={item.image} alt={item.name}></Product.Image>
                </div>
                <Product.SubTitle>{item.brand} <small className="text-primary font-weight-bold">{item.price}</small><small className="product-hover">Sepete Ekle</small></Product.SubTitle>
            </Product.Div>
            </a>
        ))}
        {FashionData.map((item)=>(
            <a key={item.id} href="/premier" className="div-link-main-page">
            <Product.Div className='electronic-product' onClick={()=> setProduct(item)}>
                <Product.Title>{item.name}</Product.Title>
                <div className="product-img-div">
                <Product.Image src={item.image} alt={item.name}></Product.Image>
                </div>
                <Product.SubTitle>{item.brand} <small className="text-primary font-weight-bold">{item.price}</small> <small className="product-hover">Sepete Ekle</small></Product.SubTitle>
            </Product.Div>
            </a>
        ))}
         </div>

            </div>
    </Product>       
            </div>
        )
    }
}

export default BestOppertunity
