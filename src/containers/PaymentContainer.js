import React from 'react'

const PaymentContainer = () => {
    return (
        <div className="container payment-container">
            <div className="payment-warning text-center">
                <small>Amazon Clone Eğitim Amaçlı Yazılmıştır. Bu Yüzden Ödeme Kısmı " Kötüye Kullanılabilme Riski " Göz Önüne Alınarak Yazılmamıştır...</small>
                <br></br>
                <br></br>
                <br></br>
                <small>Anlayışın için teşekkürler...</small>
            </div>
            <div className="special-container">
                
            </div>
        </div>
    )
}

export default PaymentContainer
