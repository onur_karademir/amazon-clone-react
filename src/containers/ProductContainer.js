import React from 'react'
import { Product } from '../components'
import Data from "../data/data.js"
import electronicData from "../data/electronicData.js"
const ProductContainer = () => {
    let productArray = [];
    function setProduct (item) {
            productArray.push({
                id: `${item.id}`,
                name: `${item.name}`,
                img: `${item.image}`,
                brand: `${item.brand}`,
                price:`${item.price}`,
                rating:`${item.rating}`,
                numReviews:`${item.numReviews}`,
                subTitle:`${item.subTitle}`
            })
            localStorage.setItem("Product", JSON.stringify(productArray));
    }
    return (
        <Product fluid={true} className="main-page-prod">
            {Data.map((item)=>(
                <a key={item.id} href="/premier" className="div-link-main-page">
                <Product.Div onClick={()=> setProduct(item)} className="item-div-main-page">
                    <Product.Title>{item.name}</Product.Title>
                    <div className="product-img-div">
                    <Product.Image src={item.image} alt={item.name}></Product.Image>
                    </div>
                    <Product.SubTitle>{item.brand} <small className="text-primary font-weight-bold">{item.price}</small><small className="hover-content">Sepete Ekle</small></Product.SubTitle>
                    
                </Product.Div>
                </a>
            ))}
            {electronicData.map((item)=>(
                <a key={item.id} href="/premier" className="div-link-main-page">
                <Product.Div onClick={()=> setProduct(item)} className="item-div-main-page">
                    <Product.Title>{item.name}</Product.Title>
                    <div className="product-img-div">
                    <Product.Image src={item.image} alt={item.name}></Product.Image>
                    </div>
                    <Product.SubTitle>{item.brand} <small className="text-primary font-weight-bold">{item.price}</small><small className="hover-content">Sepete Ekle</small></Product.SubTitle>
                </Product.Div>
                </a>
            ))}
        </Product>
    )
}

export default ProductContainer
