import React from 'react'
import {ContainerWrapper,Div,Image,Title,SubTitle} from './Product.El'

const Product = ({children,...restProps}) => {
    return (
        <ContainerWrapper {...restProps}>{children}</ContainerWrapper>
    )
}
Product.ContainerWrapper = function ProductContainerWrapper({children,...restProps}){
    return <ContainerWrapper {...restProps}>{children}</ContainerWrapper>
} 
Product.Div = function ProductDiv({children,...restProps}){
    return <Div {...restProps}>{children}</Div>
} 
Product.Image = function ProductImage({children,...restProps}){
    return <Image {...restProps}>{children}</Image>
} 
Product.Title = function ProductTitle({children,...restProps}){
    return <Title {...restProps}>{children}</Title>
} 
Product.SubTitle = function ProductSubTitle({children,...restProps}){
    return <SubTitle {...restProps}>{children}</SubTitle>
} 
export default Product