import React from 'react'
import {ContainerWrapper,Div} from './Footer.EL'

const Footer = ({children,...restProps}) => {
    return (
        <ContainerWrapper {...restProps}>{children}</ContainerWrapper>
    )
}
Footer.ContainerWrapper = function FooterContainerWrapper({children,...restProps}){
    return <ContainerWrapper {...restProps}>{children}</ContainerWrapper>
} 
Footer.Div = function FooterDiv({children,...restProps}){
    return <Div {...restProps}>{children}</Div>
} 
export default Footer