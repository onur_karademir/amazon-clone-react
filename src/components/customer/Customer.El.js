import styled from "styled-components/macro";
import {Container} from "reactstrap"

export const ContainerWrapper = styled(Container)`
background-color:#ffffff;
font-family: "Amazon Ember",Arial,sans-serif !important;
`
export const WarningDiv = styled.div`
width: 100%;
border: 1px solid #8c6e00;
padding: 5px 30px;
font-family: "Amazon Ember",Arial,sans-serif !important;
margin: 1rem 0;
`
export const ExplainDiv = styled.div`
margin: 1rem 0;
width: 100%;
padding: 10px 30px;
font-family: "Amazon Ember",Arial,sans-serif !important;
`
export const Title = styled.h3`
color: #0F1111;
`
export const SubTitle = styled.p`
color:#111;
font-size: 13px;
line-height: 19px;
`
export const Text = styled.p`

`