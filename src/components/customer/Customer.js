import React from "react";

import {
  ContainerWrapper,
  WarningDiv,
  ExplainDiv,
  Title,
  SubTitle,
  Text,
} from "./Customer.El";

const Customer = ({ children, ...restProps }) => {
  return <ContainerWrapper {...restProps}>{children}</ContainerWrapper>;
};

Customer.ContainerWrapper = function CustomerContainerWrapper({
  children,
  ...restProps
}) {
  return <ContainerWrapper {...restProps}>{children}</ContainerWrapper>;
};
Customer.WarningDiv = function CustomerWarningDiv({ children, ...restProps }) {
  return <WarningDiv {...restProps}>{children}</WarningDiv>;
};
Customer.ExplainDiv = function CustomerExplainDiv({ children, ...restProps }) {
  return <ExplainDiv {...restProps}>{children}</ExplainDiv>;
};
Customer.Title = function CustomerTitle({ children, ...restProps }) {
  return <Title {...restProps}>{children}</Title>;
};
Customer.SubTitle = function CustomerSubTitle({ children, ...restProps }) {
  return <SubTitle {...restProps}>{children}</SubTitle>;
};
Customer.Text = function CustomerText({ children, ...restProps }) {
  return <Text {...restProps}>{children}</Text>;
};

export default Customer;
