import styled from "styled-components/macro";
import { Container } from "reactstrap";
export const ContainerWrapper = styled(Container)``;

export const BestSellerDiv = styled.div`
  width: 100%;
  border-bottom: 3px solid grey;
  padding: 20px 30px 30px 30px;
  :last-of-type{
        margin-bottom: 50px;
  }
`;
export const BestSellerItem = styled.div`
  width: 200px;
  height: 200px;
  display: inline-block;
  position: relative;
`;
export const BestSellerImage = styled.img`
  object-fit: contain;
  max-width: 200px;
  width: 100%;
  max-height: 160px;
  height: 100%;
`;
export const BestSellerTitle = styled.h4`
text-align:left;
border-bottom: 2px solid #232f3e;
display:inline-block;
`;

export const BestSellerSubTitle = styled.p`
  margin: 0 !important;
  text-align: center;
`;
export const BestSellerNumber = styled.p`
  position: absolute;
  top: 0;
  left: 0;
  font-weight: 700;
  color: #232f3e;
  font-size: 16px;
`;
