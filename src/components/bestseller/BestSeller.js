import React from 'react'
import { ContainerWrapper,BestSellerDiv,BestSellerItem,BestSellerImage,BestSellerTitle,BestSellerSubTitle,BestSellerNumber } from './BestSeller.EL'

const BestSeller = ({children,...restProps}) => {
    return (
        <ContainerWrapper {...restProps}>{children}</ContainerWrapper>
    )
}

BestSeller.ContainerWrapper = function BestSellerContainerWrapper({children,...restProps}) {
    return <ContainerWrapper {...restProps}>{children}</ContainerWrapper>
}
BestSeller.BestSellerDiv = function BestSellerBestSellerDiv({children,...restProps}) {
    return <BestSellerDiv {...restProps}>{children}</BestSellerDiv>
}
BestSeller.BestSellerItem = function BestSellerBestSellerItem({children,...restProps}) {
    return <BestSellerItem {...restProps}>{children}</BestSellerItem>
}
BestSeller.BestSellerImage = function BestSellerBestSellerImage({children,...restProps}) {
    return <BestSellerImage {...restProps}>{children}</BestSellerImage>
}
BestSeller.BestSellerTitle = function BestSellerBestSellerTitle({children,...restProps}) {
    return <BestSellerTitle {...restProps}>{children}</BestSellerTitle>
}
BestSeller.BestSellerSubTitle = function BestSellerBestSellerSubTitle({children,...restProps}) {
    return <BestSellerSubTitle {...restProps}>{children}</BestSellerSubTitle>
}
BestSeller.BestSellerNumber = function BestSellerBestSellerNumber({children,...restProps}) {
    return <BestSellerNumber {...restProps}>{children}</BestSellerNumber>
}

export default BestSeller
