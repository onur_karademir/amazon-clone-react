import styled from "styled-components/macro"
import {Container} from "reactstrap"
export const ContainerWrapper = styled(Container)`
background-color:white;
height:auto;
text-align:center;
position:relative;
margin-bottom: 40px;
`
export const Div = styled.div`
position:relative;
display:inline-block;
max-width:300px;
width:100%;
height:80px;
background-color:white;
margin:1rem 1rem;
padding:20px;
border-radius:5px;
transition:all .5s ease-in-out;
box-shadow: rgba(0, 0, 0, 0.25) 0px 54px 55px, rgba(0, 0, 0, 0.12) 0px -12px 30px, rgba(0, 0, 0, 0.12) 0px 4px 6px, rgba(0, 0, 0, 0.17) 0px 12px 13px, rgba(0, 0, 0, 0.09) 0px -3px 5px;
&:hover{
    cursor:pointer;
    background-color:#232f3e;
}
&:hover h5 {
    color:#ffff;
}
`
export const Image = styled.img`
width: 100%;
object-fit:contain;
`
export const Title = styled.h5`
color:black;
`
export const SubTitle = styled.p`
color:black;
`
