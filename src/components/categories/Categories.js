import React from 'react'
import {ContainerWrapper,Div,Image,Title,SubTitle} from './Categories.El'

const Categories = ({children,...restProps}) => {
    return (
        <ContainerWrapper {...restProps}>{children}</ContainerWrapper>
    )
}
Categories.ContainerWrapper = function CategoriesContainerWrapper({children,...restProps}){
    return <ContainerWrapper {...restProps}>{children}</ContainerWrapper>
} 
Categories.Div = function CategoriesDiv({children,...restProps}){
    return <Div {...restProps}>{children}</Div>
} 
Categories.Image = function CategoriesImage({children,...restProps}){
    return <Image {...restProps}>{children}</Image>
} 
Categories.Title = function CategoriesTitle({children,...restProps}){
    return <Title {...restProps}>{children}</Title>
} 
Categories.SubTitle = function CategoriesSubTitle({children,...restProps}){
    return <SubTitle {...restProps}>{children}</SubTitle>
} 
export default Categories