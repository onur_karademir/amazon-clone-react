import React from "react";
import {
  ContainerWrapper,
  Image,
  ImageDiv,
  TextDiv,
  BasketDiv,
  BasketButton,
  Title,
  SubTitle,
  Text,
} from "./Premier.EL";
const Premier = ({ children, ...restProps }) => {
  return <ContainerWrapper {...restProps}>{children}</ContainerWrapper>;
};

Premier.ContainerWrapper = function PremierContainerWrapper({
  children,
  ...restProps
}) {
  return <ContainerWrapper {...restProps}>{children}</ContainerWrapper>;
};
Premier.Image = function PremierImage({ children, ...restProps }) {
  return <Image {...restProps}>{children}</Image>;
};
Premier.ImageDiv = function PremierImageDiv({ children, ...restProps }) {
  return <ImageDiv {...restProps}>{children}</ImageDiv>;
};
Premier.TextDiv = function PremierTextDiv({ children, ...restProps }) {
  return <TextDiv {...restProps}>{children}</TextDiv>;
};
Premier.BasketDiv = function PremierBasketDiv({ children, ...restProps }) {
  return <BasketDiv {...restProps}>{children}</BasketDiv>;
};
Premier.BasketButton = function PremierBasketButton({ children, ...restProps }) {
  return <BasketButton {...restProps}>{children}</BasketButton>;
};
Premier.Title = function PremierTitle({ children, ...restProps }) {
  return <Title {...restProps}>{children}</Title>;
};
Premier.SubTitle = function PremierSubTitle({ children, ...restProps }) {
  return <SubTitle {...restProps}>{children}</SubTitle>;
};
Premier.Text = function PremierText({ children, ...restProps }) {
  return <Text {...restProps}>{children}</Text>;
};

export default Premier;
