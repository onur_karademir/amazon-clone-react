import styled from "styled-components/macro"

import {Container} from "reactstrap"

export const ContainerWrapper = styled(Container)`
margin:0 auto;
`

export const ImageDiv = styled.div`
max-width:400px;
width:100%auto;
padding:40px 4%;
overflow:hidden;
`
export const Image = styled.img`
width: 100%;
transition:all .5s ease-in-out;
${ImageDiv}:hover &{
    transform: scale(1.8);
  cursor: zoom-in;
}
`
export const TextDiv = styled.div`
border-bottom:1px solid grey;
padding:35px 5%;
line-height:1.6;
`
export const SubTextDiv = styled.div`
border-bottom:1px solid grey;
`
export const Title = styled.h4`
color: #232f3e;
text-align:left;
`
export const SubTitle = styled.p`

`
export const Text = styled.p`

`
export const BasketDiv = styled.div`
border:1px solid #5e6868;
border-width:thin;
padding:10px;
border-radius:4px;
`
export const BasketButton = styled.a`
background-color:#f3d078;
color:#232f3e;
width: 100%;
border-radius:4px;
padding:8px;
text-align:center;
position: relative;
outline:0;
border:none;
display: block;
font-weight:600;
&:hover{
    cursor:pointer;
    background-color:#f0a83e;
    color:#232f3e;
    text-decoration:none;
}
`