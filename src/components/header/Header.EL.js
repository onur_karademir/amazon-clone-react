import styled from "styled-components/macro";
import {Link as ReactRouterLink} from "react-router-dom";
import { Container, Row} from "reactstrap";

export const HeaderCont = styled(Container)`
background-color:#131921 !important;
padding:10px 15px 0px 15px;
width:100% !important;
`;
export const HeaderRow = styled(Row)``;
export const HeaderDiv = styled.div`
width: 160px;
display: flex;
flex-direction: row;
justify-content: center;
align-items: center;
`;
export const HeaderNavItem = styled.ul`
list-style:none;
text-decoration:none;
margin:0 !important;
padding:0 !important;
display: flex;
flex-direction:row;
align-items:center;
justify-content:space-evenly;
width:100%;
height: 50px;
`;
export const HeaderNavLi = styled.li`
list-style:none;
text-decoration:none;
`;
export const HeaderNavLink = styled(ReactRouterLink)`
text-decoration:none;
color:#ffff;
&:hover{
color:#ffff;
text-decoration:none;
cursor:pointer;
}
`;
export const Input = styled.input`
  max-width: 450px;
  width: 100%;
  border: 0;
  padding: 10px;
  height: 40px;
  box-sizing: border-box;
  &:focus {
    outline:none;
  border:none;
  }
`;
export const Button = styled.button`
  border-top-right-radius:4px;
  border-bottom-right-radius:4px;
  padding: 8px 15px !important;
  outline:none;
  border:none;
  background-color:#febd69;
  &:focus {
    outline:none;
  border:none;
  }
`;
