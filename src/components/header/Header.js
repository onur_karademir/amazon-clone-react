import React from "react";
import {FaSearch} from "react-icons/fa"
import {
  HeaderCont,
  HeaderRow,
  HeaderNavItem,
  HeaderNavLink,
  HeaderNavLi,
  HeaderDiv,
  Input,
  Button
} from "./Header.EL";
const Header = ({ children, ...restProps }) => {
  return <HeaderCont {...restProps}>{children}</HeaderCont>;
};

Header.HeaderCont = function HeaderHeaderCont({ children, ...restProps }) {
  return <HeaderCont {...restProps}>{children}</HeaderCont>;
};
Header.HeaderRow = function HeaderHeaderRow({ children, ...restProps }) {
  return <HeaderRow {...restProps}>{children}</HeaderRow>;
};
Header.HeaderNavItem = function HeaderHeaderNavItem({ children, ...restProps }) {
  return <HeaderNavItem {...restProps}>{children}</HeaderNavItem>;
};

Header.HeaderNavLink = function HeaderHeaderNavLink({ to, children, ...restProps }) {
  return <HeaderNavLink to={to} {...restProps}>{children}</HeaderNavLink>;
};
Header.HeaderNavLi = function HeaderHeaderNavLi({children, ...restProps }) {
  return <HeaderNavLi {...restProps}>{children}</HeaderNavLi>;
};
Header.HeaderDiv = function HeaderHeaderDiv({children, ...restProps }) {
  return <HeaderDiv {...restProps}>{children}</HeaderDiv>;
};
Header.Input = function HeaderInput({ ...restProps }) {
  return <Input {...restProps} />;
};
Header.Button = function HeaderButton({ children,...restProps }) {
  return <Button {...restProps}>{children} <FaSearch className="icon"/></Button>;
};
export default Header;
