import React from 'react'
import {ContainerWrapper,Item,Inner,Image,ImageDiv,Title,SubTitle} from "./Slide.El"
const Slide = ({children,...restProps}) => {
    return (
        <ContainerWrapper {...restProps}>{children}</ContainerWrapper>
        )
    }
    
Slide.ContainerWrapper = function SlideContainerWrapper ({children,...restProps}) {
    return <ContainerWrapper {...restProps}>{children}</ContainerWrapper>
}
Slide.Item = function SlideItem ({children,...restProps}) {
    return <Item {...restProps}>{children}</Item>
}
Slide.Inner = function SlideInner ({children,...restProps}) {
    return <Inner {...restProps}>{children}</Inner>
}
Slide.ImageDiv = function SlideImageDiv ({children,...restProps}) {
    return <ImageDiv {...restProps}>{children}</ImageDiv>
}
Slide.Image = function SlideImage ({children,...restProps}) {
    return <Image {...restProps}>{children}</Image>
}
Slide.Title = function SlideTitle ({children,...restProps}) {
    return <Title {...restProps}>{children}</Title>
}
Slide.SubTitle = function SlideSubTitle ({children,...restProps}) {
    return <SubTitle {...restProps}>{children}</SubTitle>
}

export default Slide
