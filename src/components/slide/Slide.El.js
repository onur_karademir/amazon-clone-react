import styled from "styled-components/macro";
export const ContainerWrapper = styled.div`
  width: 100%;
  margin: 40px 0 0 0;
`;
export const Inner = styled.div`
  overflow-x: scroll;
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
`;
export const Item = styled.div`
  display: inline-block;
  margin-left:20px;
  margin-right:20px;
`;
export const Image = styled.img``;
export const ImageDiv = styled.div``;
export const Title = styled.p``;
export const SubTitle = styled.p``;
