import React from 'react'
import {ContainerWrapper,Div,Image,Title,SubTitle} from './Electronic.El'

const Electronic = ({children,...restProps}) => {
    return (
        <ContainerWrapper {...restProps}>{children}</ContainerWrapper>
    )
}
Electronic.ContainerWrapper = function ElectronicContainerWrapper({children,...restProps}){
    return <ContainerWrapper {...restProps}>{children}</ContainerWrapper>
} 
Electronic.Div = function ElectronicDiv({children,...restProps}){
    return <Div {...restProps}>{children}</Div>
} 
Electronic.Image = function ElectronicImage({children,...restProps}){
    return <Image {...restProps}>{children}</Image>
} 
Electronic.Title = function ElectronicTitle({children,...restProps}){
    return <Title {...restProps}>{children}</Title>
} 
Electronic.SubTitle = function ElectronicSubTitle({children,...restProps}){
    return <SubTitle {...restProps}>{children}</SubTitle>
} 
export default Electronic