import styled from "styled-components/macro"
import {Container} from "reactstrap"
export const ContainerWrapper = styled(Container)`
background-color:#eaeded;
height:auto;
text-align:center;
position:relative;
margin-bottom:40px;
`
export const Div = styled.div`
text-align:center;
display:inline-block;
max-width:300px;
width:100%;
height:350px;
background-color:white;
margin:1rem 1rem;
padding:20px;
box-shadow: rgba(0, 0, 0, 0.25) 0px 54px 55px, rgba(0, 0, 0, 0.12) 0px -12px 30px, rgba(0, 0, 0, 0.12) 0px 4px 6px, rgba(0, 0, 0, 0.17) 0px 12px 13px, rgba(0, 0, 0, 0.09) 0px -3px 5px;
`
export const Image = styled.img`
    width: 100%;
    object-fit: contain;
    max-width: 300px;
    max-height: 400px;
    height: 70%;
`
export const Title = styled.h5`
color:black;
`
export const SubTitle = styled.p`
color:black;
margin-top:40px;
font-weight:700;
color: #232f3e;
`
