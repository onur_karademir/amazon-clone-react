import React from 'react'
import {ContainerWrapper,Div,Image,Title,SubTitle} from './Forme.El'

const Forme = ({children,...restProps}) => {
    return (
        <ContainerWrapper {...restProps}>{children}</ContainerWrapper>
    )
}
Forme.ContainerWrapper = function FormeContainerWrapper({children,...restProps}){
    return <ContainerWrapper {...restProps}>{children}</ContainerWrapper>
} 
Forme.Div = function FormeDiv({children,...restProps}){
    return <Div {...restProps}>{children}</Div>
} 
Forme.Image = function FormeImage({children,...restProps}){
    return <Image {...restProps}>{children}</Image>
} 
Forme.Title = function FormeTitle({children,...restProps}){
    return <Title {...restProps}>{children}</Title>
} 
Forme.SubTitle = function FormeSubTitle({children,...restProps}){
    return <SubTitle {...restProps}>{children}</SubTitle>
} 
export default Forme