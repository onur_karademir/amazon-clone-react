import styled from "styled-components/macro";
export const ContainerWrapper = styled.div`
  width: 100%;
  margin: 0 0 0 0;
  padding:30px 0;
  height:auto;
  background: linear-gradient(0deg, rgba(188,227,244,1) 0%, rgba(255,255,255,1) 80%, rgba(188,227,244,1)  100%);
  box-sizing:border-box;
  position:relative;
  `;
export const Inner = styled.div`
box-sizing:border-box;
padding:40px 0;
text-align:center;
`;
export const Item = styled.div`
box-sizing:border-box;
  display: inline-block;
  margin-left:20px;
  margin-right:20px;
  background-color:white;
  width:280px;
  height:300px;
  text-align:center;
  padding:15px;
  overflow:hidden;
  transition:all .3s ease-in-out;
`;
export const Image = styled.img`
width:100%;
object-fit:contain;
overflow:hidden;
  transition:all .3s ease-in-out;
${Item}:hover & {
    transform:scale(1.1);
    cursor:pointer;
  }
`;
export const ImageDiv = styled.div``;

export const Title = styled.p`
text-align:center;
font-weight:600;
font-size:18px;
`;
export const SubTitle = styled.p`
text-align:center;
font-weight:600;
font-size:12px;
margin-top:10px;
`;
