import React from 'react'
import {ContainerWrapper,Item,Inner,Image,ImageDiv,Title,SubTitle} from "./Offer.El"
const Offer = ({children,...restProps}) => {
    return (
        <ContainerWrapper {...restProps}>{children}</ContainerWrapper>
        )
    }
    
Offer.ContainerWrapper = function OfferContainerWrapper ({children,...restProps}) {
    return <ContainerWrapper {...restProps}>{children}</ContainerWrapper>
}
Offer.Item = function OfferItem ({children,...restProps}) {
    return <Item {...restProps}>{children}</Item>
}
Offer.Inner = function OfferInner ({children,...restProps}) {
    return <Inner {...restProps}>{children}</Inner>
}
Offer.ImageDiv = function OfferImageDiv ({children,...restProps}) {
    return <ImageDiv {...restProps}>{children}</ImageDiv>
}
Offer.Image = function OfferImage ({children,...restProps}) {
    return <Image {...restProps}>{children}</Image>
}
Offer.Title = function OfferTitle ({children,...restProps}) {
    return <Title {...restProps}>{children}</Title>
}
Offer.SubTitle = function OfferSubTitle ({children,...restProps}) {
    return <SubTitle {...restProps}>{children}</SubTitle>
}

export default Offer
