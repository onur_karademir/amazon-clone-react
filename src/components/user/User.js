import React from 'react'
import {ContainerWrapper,ItemDiv,ItemImage,ItemTitle,ItemSubtitle,Text} from "./User.El"
const User = ({children,...restProps}) => {
    return (
        <ContainerWrapper {...restProps}>{children}</ContainerWrapper>
    )
}

User.ContainerWrapper = function UserContainerWrapper ({children,...restProps}) {
return <ContainerWrapper {...restProps}>{children}</ContainerWrapper>
}
User.ItemDiv = function UserItemDiv ({children,...restProps}) {
return <ItemDiv {...restProps}>{children}</ItemDiv>
}
User.ItemImage = function UserItemImage ({children,...restProps}) {
return <ItemImage {...restProps}>{children}</ItemImage>
}
User.ItemTitle = function UserItemTitle ({children,...restProps}) {
return <ItemTitle {...restProps}>{children}</ItemTitle>
}
User.ItemSubtitle = function UserItemSubtitle ({children,...restProps}) {
return <ItemSubtitle {...restProps}>{children}</ItemSubtitle>
}
User.Text = function UserText ({children,...restProps}) {
return <Text {...restProps}>{children}</Text>
}

export default User
