import styled from "styled-components/macro";
import {Container} from "reactstrap";
export const ContainerWrapper = styled(Container)`
padding: 40px 4%;
box-sizing:border-box;
font-family: "Amazon Ember",Arial,sans-serif !important;
`
export const ItemDiv = styled.div`
max-width:250px;
max-height:150px;
width: 100%;
height: 100%;
background-color:white;
margin: 20px 40px;
border:1px solid #dddddd;
box-sizing:border-box;
display:inline-block;
text-align:left;
padding: 10px;
border-radius:4px;
transition:all .3s ease-in-out;
&:hover {
    cursor:pointer;
    background-color: #dddddd;
}
`
export const ItemImage = styled.img`
width:100% ;
`
export const ItemTitle = styled.h5`
display:block;
font-size:12px;
`
export const ItemSubtitle = styled.p`
display:block;

`
export const Text = styled.p`
display:block;

`