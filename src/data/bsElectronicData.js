export default [
    {
        id: '1',
        name: 'Playstation 5',
        category: 'Electronik',
        image: '/images/playstation.jpg',
        price: "8299 TL",
        brand: ' Sony',
        rating: 4.9,
        numReviews: 119,
        subTitle: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia,"
      },
      {
        id: '2',
        name: 'Xiaomi Mi Box 4k',
        category: 'Electronik',
        image: '/images/mibox.jpg',
        price: "499 TL",
        brand: 'Xiaomi Mi',
        rating: 4.7,
        numReviews: 5,
        subTitle: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia,"
      },
      {
        id: '3',
        name: 'İphone 11',
        category: 'Electronik',
        image: '/images/iphone.jpg',
        price: "7999 TL",
        brand: 'Apple',
        rating: 3.5,
        numReviews: 98,
        subTitle: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia,"
      }
]