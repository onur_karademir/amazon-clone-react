export default [
  {
    id: '7',
    name: 'Playstation 5',
    category: 'Electronik',
    image: '/images/playstation.jpg',
    price: "8299 TL",
    brand: ' Sony',
    rating: 4.9,
    numReviews: 119,
    subTitle: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia,"
  },
  {
    id: '8',
    name: 'Xiaomi Mi Box 4k',
    category: 'Electronik',
    image: '/images/mibox.jpg',
    price: "499 TL",
    brand: 'Xiaomi Mi',
    rating: 4.7,
    numReviews: 5,
    subTitle: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia,"
  },
  {
    id: '9',
    name: 'İphone 11',
    category: 'Electronik',
    image: '/images/iphone.jpg',
    price: "7999 TL",
    brand: 'Apple',
    rating: 3.5,
    numReviews: 98,
    subTitle: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia,"
  }, 
  {
    id: '10',
    name: 'Xbox Series S',
    category: 'Electronik',
    image: '/images/xbox.jpg',
    price:"4999 TL",
    brand: 'Microsoft',
    rating: 3.5,
    numReviews: 118,
    subTitle: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia,"
  },
  {
    id: '11',
    name: 'Apple Watch',
    category: 'Electronik',
    image: '/images/watch.jpg',
    price:"1299 TL",
    brand: 'Apple',
    rating: 4.5,
    numReviews: 49,
    subTitle: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia,"
  },
  {
    id: '12',
    name: 'Samsung a7',
    category: 'Electronik',
    image: '/images/samsung.jpg',
    price:"1999 TL",
    brand: 'Samsung',
    rating: 4.9,
    numReviews: 148,
    subTitle: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia,"
  },
]