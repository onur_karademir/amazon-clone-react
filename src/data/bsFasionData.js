export default [
    {
        id: '1',
        name: 'Slim Shirt',
        category: 'Shirts',
        image: '/images/d1.jpg',
        price: "60 TL",
        brand: ' Nike',
        rating: 4.1,
        numReviews: 10,
        subTitle: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia,"
      },
      {
        id: '2',
        name: 'Fit Shirt',
        category: 'Shirts',
        image: '/images/d2.jpg',
        price: "50 TL",
        brand: 'Beşiktaş Cumartesi Pazarı',
        rating: 4.7,
        numReviews: 5,
        subTitle: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia,"
      },
      {
        id: '3',
        name: 'Best Pants',
        category: 'Pants',
        image: '/images/d3.jpg',
        price: "70 TL",
        brand: 'Çubuklu Sosyete Çarşısı',
        rating: 3.5,
        numReviews: 8,
        subTitle: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia,"
      }
]