import React, { Component } from 'react'
import BestSellerContainer from '../containers/BestSellerContainer'
import FooterContainer from '../containers/FooterContainer'
import HeaderContainer from '../containers/HeaderContainer'
import OfferContainer from '../containers/OfferContainer'
import SlideContainer from '../containers/SlideContainer'

class BestSeller extends Component {
    render() {
        return (
            <>
            <HeaderContainer></HeaderContainer>
            <BestSellerContainer></BestSellerContainer>
            <SlideContainer></SlideContainer>
            <OfferContainer></OfferContainer>
            <FooterContainer></FooterContainer>
            </>
        )
    }
}

export default BestSeller
