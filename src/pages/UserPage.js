import React from 'react'
import FooterContainer from '../containers/FooterContainer'
import HeaderContainer from '../containers/HeaderContainer'
import UserContainer from '../containers/UserContainer'
import OfferContainer from '../containers/OfferContainer'
import SlideContainer from '../containers/SlideContainer'

const UserPage = () => {
    return (
        <>
          <HeaderContainer></HeaderContainer>
          <UserContainer></UserContainer>
          <SlideContainer></SlideContainer>
          <OfferContainer></OfferContainer>
          <FooterContainer></FooterContainer>  
        </>
    )
}

export default UserPage
