import React, { Component } from 'react'
import CustomerContainer from '../containers/CustomerContainer'
import HeaderContainer from '../containers/HeaderContainer'
import FooterContainer from "../containers/FooterContainer";
import OfferContainer from '../containers/OfferContainer'
import SlideContainer from '../containers/SlideContainer'
class Customer extends Component {
    render() {
        return (
            <>
            <HeaderContainer></HeaderContainer>
            <CustomerContainer></CustomerContainer>
            <SlideContainer></SlideContainer>
            <OfferContainer></OfferContainer>
            <FooterContainer></FooterContainer>
            </>
        )
    }
}

export default Customer
