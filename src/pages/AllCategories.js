import React from 'react'
import CategoriesContainer from '../containers/CategoriesContainer'
import FooterContainer from '../containers/FooterContainer'
import HeaderContainer from '../containers/HeaderContainer'
import OfferContainer from '../containers/OfferContainer'
import SlideContainer from '../containers/SlideContainer'

const AllCategories = () => {
    return (
        <>
        <HeaderContainer></HeaderContainer>
        <CategoriesContainer></CategoriesContainer>
        <SlideContainer></SlideContainer>
        <OfferContainer></OfferContainer>
        <FooterContainer></FooterContainer>
        </>
    )
}

export default AllCategories
