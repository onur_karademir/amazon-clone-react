import React from 'react'
import FooterContainer from '../containers/FooterContainer'
import HeaderContainer from '../containers/HeaderContainer'
import PremierContainer from '../containers/PremierContainer'

const Premier = () => {
    return (
        <>
        <HeaderContainer></HeaderContainer>
        <PremierContainer></PremierContainer>
        <FooterContainer></FooterContainer>
        </>
    )
}

export default Premier
