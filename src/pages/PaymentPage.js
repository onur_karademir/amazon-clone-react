import React from "react";
import FooterContainer from "../containers/FooterContainer";
import HeaderContainer from "../containers/HeaderContainer";
import PaymentContainer from "../containers/PaymentContainer";
const PaymentPage = () => {
  return (
    <>
      <HeaderContainer></HeaderContainer>
      <PaymentContainer></PaymentContainer>
      <FooterContainer></FooterContainer>
    </>
  );
};

export default PaymentPage;
