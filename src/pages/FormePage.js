import React from 'react'
import FooterContainer from '../containers/FooterContainer'
import FormeContainer from '../containers/FormeContainer'
import HeaderContainer from '../containers/HeaderContainer'
import OfferContainer from '../containers/OfferContainer'
import SlideContainer from '../containers/SlideContainer'
const FormePage = () => {
    return (
        <>
        <HeaderContainer></HeaderContainer>
        <FormeContainer></FormeContainer>
        <SlideContainer></SlideContainer>
        <OfferContainer></OfferContainer>
        <FooterContainer></FooterContainer>
        </>
    )
}

export default FormePage
