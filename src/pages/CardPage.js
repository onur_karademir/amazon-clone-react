import React from "react";
import CardContainer from "../containers/CardContainer";
import FooterContainer from "../containers/FooterContainer";
import HeaderContainer from "../containers/HeaderContainer";
import OfferContainer from "../containers/OfferContainer";
import SlideContainer from "../containers/SlideContainer";

const CardPage = () => {
  return (
    <>
      <HeaderContainer></HeaderContainer>
      <CardContainer></CardContainer>
      <SlideContainer></SlideContainer>
      <OfferContainer></OfferContainer>
      <FooterContainer></FooterContainer>
    </>
  );
};

export default CardPage;
