import React from 'react'
import HeaderContainer from '../containers/HeaderContainer'
import ElectronicContainer from "../containers/ElectronicContainer";
import FooterContainer from "../containers/FooterContainer";
import SlideContainer from '../containers/SlideContainer';
import OfferContainer from '../containers/OfferContainer';

const Electronics = () => {
    return (
        <>
        <HeaderContainer></HeaderContainer>
        <ElectronicContainer></ElectronicContainer>
        <SlideContainer></SlideContainer>
        <OfferContainer></OfferContainer>
        <FooterContainer></FooterContainer>
        </>
    )
}

export default Electronics
