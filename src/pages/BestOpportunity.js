import React from 'react'
import BestOppertunity from '../containers/BestOppertunity'
import FooterContainer from '../containers/FooterContainer'
import HeaderContainer from '../containers/HeaderContainer'
import OfferContainer from '../containers/OfferContainer'
import SlideContainer from '../containers/SlideContainer'

const BestOpportunity = () => {
    return (
        <>
         <HeaderContainer></HeaderContainer>
         <BestOppertunity></BestOppertunity>
         <SlideContainer></SlideContainer>
         <OfferContainer></OfferContainer>
         <FooterContainer></FooterContainer>   
        </>
    )
}

export default BestOpportunity
