import React from 'react'
import FooterContainer from '../containers/FooterContainer'
import HeaderContainer from '../containers/HeaderContainer'
import PrimeContainer from '../containers/PrimeContainer'
const PrimePage = () => {
    return (
        <>
        <HeaderContainer></HeaderContainer>
        <PrimeContainer></PrimeContainer>
        <FooterContainer></FooterContainer>
        </>
    )
}

export default PrimePage
