import React from "react";
import HeaderContainer from "../containers/HeaderContainer";
import Carrosel from "../containers/Carrosel"
import ProductContainer from "../containers/ProductContainer";
import FooterContainer from "../containers/FooterContainer";
import SlideContainer from "../containers/SlideContainer";
import OfferContainer from "../containers/OfferContainer";

const Home = () => {
  return (
    <>
      <HeaderContainer></HeaderContainer>
      <Carrosel></Carrosel>
      <ProductContainer></ProductContainer>
      <SlideContainer></SlideContainer>
      <OfferContainer></OfferContainer>
      <FooterContainer></FooterContainer>
    </>
  );
};

export default Home;
